import 'package:flutter/material.dart';
import 'package:flutter_baselib/flutter_baselib.dart';
import 'package:flutter_baselib_example/common/util/ui_utils.dart';

///@date:  2021/3/10 09:32
///@author:  lixu
///@description: 相关工具类测试页面
class ToolsPage extends StatefulWidget {
  @override
  _ToolsPageState createState() => _ToolsPageState();
}

class _ToolsPageState extends State<ToolsPage> {
  String? _appInfo;
  String? _deviceInfo;

  @override
  void initState() {
    super.initState();
    _getAppInfo();
  }

  void _getAppInfo() async {
    _appInfo = '应用名称：${await PackageUtils.getAppName()}\n'
        '应用包名：${await PackageUtils.getPackageName()}\n'
        'VersionName：${await PackageUtils.getVersionName()}\n'
        'VersionCode：${await PackageUtils.getBuildNumber()}\n';

    _deviceInfo = '品牌：${await DeviceUtils.getBrand()}\n'
        '型号：${await DeviceUtils.getModel()}\n'
        '版本：${await DeviceUtils.getOsVersion()}\n';
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('测试工具类'),
      ),
      body: Container(
        alignment: Alignment.center,
        child: Column(
          children: [
            Text('$_appInfo'),
            Text('$_deviceInfo'),
            UIUtils.getButton('获取存储权限', () async {
              bool isDefine = await PermissionUtils.requestPermission(Permission.storage);
              if (isDefine) {
                ToastUtils.show('已获取存储权限');
              } else {
                ToastUtils.show('存储权限未开启');
              }
            }),
            UIUtils.getButton('检测更新应用', () async {
              FlutterBugly.checkUpgrade();
            }),
          ],
        ),
      ),
    );
  }
}
