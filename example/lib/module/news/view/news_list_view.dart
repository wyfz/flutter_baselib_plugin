import 'package:flutter/material.dart';
import 'package:flutter_baselib/flutter_baselib.dart';
import 'package:flutter_baselib_example/common/util/test_utils.dart';
import 'package:flutter_baselib_example/module/news/view_model/news_view_model.dart';
import 'package:flutter_baselib_example/module/userlist/view/item/user_item.dart';
import 'package:flutter_baselib_example/module/userlist/view_model/user_list_view_model.dart';
import 'package:flutter_easyrefresh/easy_refresh.dart';

import 'item/news_item.dart';

///@date:  2021/10/8 11:48
///@author:  lixu
///@description: 分页加载和下拉刷新功能演示页面ListView
class NewsListView extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    TestUtils.logBuild('NewsListView-build');

    return Consumer<NewsViewModel>(
      builder: (context, viewModel, child) {
        return EasyRefresh(
          footer: BallPulseFooter(color: Color(0xFFCCCCCC)),
          child: ListView.separated(
            key: PageStorageKey(1),
            padding: EdgeInsets.symmetric(horizontal: 15),
            separatorBuilder: (BuildContext context, int index) {
              return Divider(
                height: 0.5,
                color: Color(0xFFBFBFBF),
              );
            },
            itemBuilder: (context, index) {
              return NewsItem(viewModel.dataList![index]);
            },
            itemCount: viewModel.dataList!.length,
          ),
          onLoad: _onLoad(context, viewModel),
          onRefresh: () async {
            await viewModel.onPullToRefresh(context);
          },
        );
      },
    );
  }

  _onLoad(context, NewsViewModel viewModel) {
    if (viewModel.canLoadMore) {
      return () async {
        await viewModel.onLoadingMore(context);
      };
    } else {
      return null;
    }
  }
}
