import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_baselib/flutter_baselib.dart';
import 'package:flutter_baselib_example/module/news/view_model/news_view_model.dart';

import 'news_list_view.dart';

///@date:  2021/10/8 11:48
///@author:  lixu
///@description: 分页加载和下拉刷新功能演示页面
class NewsPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('头条新闻(下拉和分页加载)')),
      body: ChangeNotifierProvider(
        create: (_) {
          return NewsViewModel();
        },
        child: BaseView<NewsViewModel>(
          child: NewsListView(),
        ),
      ),
    );
  }
}
