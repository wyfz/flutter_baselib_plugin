import 'package:flutter_baselib/flutter_baselib.dart';
import 'package:flutter_baselib_example/common/net/http_urls.dart';
import 'package:flutter_baselib_example/module/news/model/news_bean.dart';

///@date:  2021/10/8 11:51
///@author:  lixu
///@description:  分页加载和下拉刷新功能演示
class NewsViewModel extends BaseViewModel<NewsBean> {
  ///********************TODO 下面方法，根据业务需求进行重写 start*************************
  ///进入页面时onLoading方法请求数据类型是否是List数据结构
  ///否则就是单个对象，优先取构造函数传递的参数
  ///子类根据需要可以选择在构造传入或重写该方法
  ///默认是List数据结构
  @override
  bool configIsRequestListData() {
    return true;
  }

  ///进入页面时onLoading方法请求的List数据是否是分页加载
  ///子类根据需要可以选择在构造传入或重写该方法
  ///_isRequestListData=true时，该方法才有效
  ///默认是分页加载
  @override
  bool configIsPageLoad() {
    return true;
  }

  ///进入页面时onLoading方法请求的List数据分页加载时，起始页Index
  ///子类根据需要可以选择在构造传入或重写该方法
  ///_isRequestListData=true并且_isPageLoad=true 时，该方法才有效
  @override
  int configInitPageIndex() {
    return 1;
  }

  ///进入页面时onLoading方法请求的List数据分页加载时，页大小
  ///子类根据需要可以选择在构造传入或重写该方法
  ///_isRequestListData=true并且_isPageLoad=true 时，该方法才有效
  @override
  int configPageSize() {
    return 20;
  }

  ///配置当前请求Options
  ///子类根据需要可以重写该方法
  @override
  Options? configHttpOptions() {
    return Options(
      contentType: XApi.contentTypeForm,
    );
  }

  ///配置下拉刷新失败时，是否显示失败的占位View
  ///场景：数据请求成功过，但是在下拉刷新时失败了，此时是否显示失败页
  ///true：显示失败页面
  ///false: 不显示失败页面，继续显示之前请求成功过的数据页面
  ///子类根据需要可以重写该方法
  @override
  bool configIsPullRefreshFailShowFailView() {
    return true;
  }

  ///配置加载更多（分页加载）失败时，是否显示失败的占位View
  ///场景：数据请求成功过，但是在加载更多时失败了，此时是否显示失败页
  ///true：显示失败页面
  ///false: 不显示失败页面，继续显示之前请求成功过的数据页面
  ///子类根据需要可以重写该方法
  @override
  bool configIsLoadMoreFailShowFailView() {
    return false;
  }

  ///********************TODO 上面方法，根据业务需求进行重写 end*************************

  ///获取http请求参数
  @override
  Map<String, dynamic> getRequestParams() {
    return {
      "page": pageNum,
      "page_size": pageSize,
      "type": "top",
      "key": "7bdc3f62f5386911bc0a748b34a86049",
    };
  }

  @override
  String getTag() {
    return "NewsViewModel";
  }

  ///获取http请求url
  @override
  String getUrl() {
    return HttpUrls.newsUrl;
  }
}
