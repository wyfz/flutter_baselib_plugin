import 'package:flutter_baselib/flutter_baselib.dart';
import 'package:flutter_baselib/src/net/model/http_result_base_bean.dart';

///@date:  2021/2/25
///@author:  lixu
///@description: http响应对象解析封装
class HttpResultBean<T> extends HttpResultBaseBean {
  ///http响应的数据是否是List数据结构
  bool isRespListData = false;

  ///响应对象
  T? data;

  ///响应List数据对象
  List<T>? dataList;

  ///响应完整json
  Map<String, dynamic>? json;

  HttpResultBean() : super(code: HttpCode.defaultCode);

  HttpErrorBean obtainErrorBean() {
    return HttpErrorBean(code: code, message: message);
  }
}
