import 'dart:convert';

import 'package:flutter_baselib/flutter_baselib.dart';

///@date:  2021/2/25 14:22
///@author:  lixu
///@description: 日志拦截器
class LogsInterceptors extends InterceptorsWrapper {
  String _tag = 'HttpLog';

  @override
  void onRequest(RequestOptions options, RequestInterceptorHandler handler) {
    LogUtils.i(_tag, "****************onRequest() start*******************");
    LogUtils.i(_tag, 'RequestHashCode：${options.hashCode}');
    LogUtils.i(_tag, 'url：${_getUrl(options)}');
    LogUtils.i(_tag, 'headers: ${json.encode(options.headers)}');
    LogUtils.i(_tag, 'method: ${options.method}');
    LogUtils.i(_tag, 'responseType: ${options.responseType.toString()}');
    LogUtils.i(_tag, 'followRedirects: ${options.followRedirects}');
    LogUtils.i(_tag, 'connectTimeout: ${options.connectTimeout}');
    LogUtils.i(_tag, 'receiveTimeout: ${options.receiveTimeout}');
    LogUtils.i(_tag, 'extra: ${json.encode(options.extra)}');
    LogUtils.i(_tag, 'queryParameters: ${json.encode(options.queryParameters)}');
    LogUtils.i(_tag, 'params: ${json.encode(options.data ?? {})}');
    LogUtils.i(_tag, "****************onRequest() end*********************");
    super.onRequest(options, handler);
  }

  @override
  void onResponse(Response response, ResponseInterceptorHandler handler) {
    LogUtils.i(_tag, "****************onResponse() start******************");
    _printResponse(response);
    LogUtils.i(_tag, "****************onResponse() end********************");
    super.onResponse(response, handler);
  }

  void _printResponse(Response response) {
    LogUtils.i(_tag, 'RequestHashCode：${response.requestOptions.hashCode}');
    LogUtils.i(_tag, 'url: ${_getUrl(response.requestOptions)}');
    LogUtils.i(_tag, 'statusCode: ${response.statusCode} ，statusMessage：${response.statusMessage}');
    if (response.isRedirect == true) {
      LogUtils.i(_tag, 'redirect: ${response.realUri}');
    }
    LogUtils.i(_tag, 'response headers: ${response.headers.toString()}');
    LogUtils.i(_tag, 'response extra: ${json.encode(response.extra)}');
    LogUtils.i(_tag, 'response text: ${response.toString()}');
  }

  String _getUrl(RequestOptions requestOptions) {
    String path = requestOptions.path;
    if (!path.startsWith(Constants.httpStartWith)) {
      return requestOptions.baseUrl + path;
    } else {
      return path;
    }
  }

  @override
  void onError(DioError err, ErrorInterceptorHandler handler) {
    LogUtils.e(_tag, "****************onError() start*********************");
    LogUtils.e(_tag, '请求异常: ${err.toString()}');
    LogUtils.e(_tag, '请求异常信息: ${err.response?.toString()}');
    LogUtils.e(_tag, "****************onError() end***********************");
    super.onError(err, handler);
  }
}
