///@date:  2021/2/25 10:20
///@author:  lixu
///@description: bugly配置，下面定义的字段都是bugly初始化时需要的字段
///参考bugly文档配置下面参数
class BuglyConfig {
  ///AppID
  String? androidAppId;
  String? iOSAppId;

  ///自定义渠道标识
  String? channel;

  ///是否自动检测更新
  bool autoCheckUpgrade = true;

  ///是否自动初始化
  bool autoInit = true;

  ///wifi下是否自动下载更新包
  bool autoDownloadOnWifi = false;

  ///是否开启热更功能
  bool enableHotfix = false;

  ///设置是否显示消息通知
  bool enableNotification = false;

  ///设置开启显示打断策略
  bool showInterruptedStrategy = true;

  ///设置是否显示弹窗中的apk信息
  bool canShowApkInfo = true;

  ///延迟初始化;单位秒
  int initDelay = 3;

  ///升级检查周期设置;单位秒
  int upgradeCheckPeriod = 20;

  ///UpgradeInfo为null时，再次check的次数，经测试1为最佳
  int checkUpgradeCount = 1;

  /// 是否自定义升级
  bool customUpgrade = false;

  ///android默认的构造方法
  BuglyConfig.defaultAndroid(this.androidAppId);

  BuglyConfig({
    this.androidAppId,
    this.iOSAppId,
    this.channel,
    required this.autoCheckUpgrade,
    required this.autoInit,
    required this.autoDownloadOnWifi,
    required this.enableHotfix,
    required this.enableNotification,
    required this.showInterruptedStrategy,
    required this.canShowApkInfo,
    required this.initDelay,
    required this.upgradeCheckPeriod,
    required this.checkUpgradeCount,
    required this.customUpgrade,
  });
}
